import React from 'react';

let UiSelect = props => {
    let copyProps = { ...props };
    delete copyProps.item;
    return (
        <select {...copyProps}>
            {props.item.map((d, i) => (
                <option value={d} key={i}>
                    {d}
                </option>
            ))}
        </select>
    );
};

export default UiSelect;
