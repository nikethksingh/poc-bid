import React from 'react';

let UiButton = props => <button {...props}>{props.text}</button>;

export default UiButton;
