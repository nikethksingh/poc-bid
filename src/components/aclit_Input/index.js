import React from 'react';

let UiInput = props => {
    return (
        <label>
            {props.label}

            <input {...props} />
        </label>
    );
};

export default UiInput;
