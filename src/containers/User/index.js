import React, { Component } from 'react';
import { connect } from 'react-redux';
import UiInput from '../../components/aclit_Input';
import UiButton from '../../components/aclit_Button';
import UiSelect from '../../components/aclit_UiSelect';

let selectData = ['computer', 'keyboard', 'mouse', 'headphone', 'laptop', 'battery'];
class User extends Component {
    state = {
        item: 'computer',
        quantity: '',
        bidAmnt: '',
    };

    handleChange = e => {
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    handleSubmit = e => {
        e.preventDefault();
        let { state, props } = this;
        let obj = { ...state };
        obj.status = false;
        obj.name = props.auth.user.name;
        this.props.saveData(obj);
        alert('bid submitted');
        this.setState({ item: '', quantity: '', bidAmnt: '' });
    };

    render() {
        let inputProps = { type: 'number', min: '0', onChange: this.handleChange };
        console.log(this.props);
        return (
            <div>
                <form onSubmit={this.handleSubmit} className="user-form">
                    <label>
                        Please select Item
                        <br />
                        <UiSelect value={this.state.item} name="item" onChange={this.handleChange} item={selectData} />
                    </label>

                    <div>
                        <UiInput {...inputProps} label={'Quantity'} value={this.state.quantity} name="quantity" required />

                        <UiInput {...inputProps} label={'Bid Amount'} value={this.state.bidAmnt} name="bidAmnt" required />
                    </div>
                    <UiButton type="submit" text="submit" />
                </form>
            </div>
        );
    }
}

let mapStateToProps = state => ({
    auth: state.auth,
    user: state.user,
});

let mapDispatchToProps = dispatch => {
    return {
        saveData: data => dispatch({ type: 'SAVE_DATA', data: data }),
        login: data => dispatch({ type: 'LOGIN', data: data }),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(User);
