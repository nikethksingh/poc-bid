import React, { Component } from 'react';
import { connect } from 'react-redux';
import UiInput from '../../components/aclit_Input';
import UiButton from '../../components/aclit_Button';

class Login extends Component {
    state = {
        value: '',
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.login(this.state.value);
    };

    handleChange = e => {
        this.setState({ value: e.target.value });
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit} className="login-form">
                <UiInput
                    type="text"
                    name="name"
                    label="Name"
                    placeholder="Please Enter your name"
                    onChange={this.handleChange}
                    required
                />
                <UiButton type="submit" text="Login" />
            </form>
        );
    }
}

let mapStateToProps = state => ({
    auth: state.auth,
});

let mapDispatchToProps = dispatch => {
    return {
        login: data => dispatch({ type: 'LOGIN', data: data }),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);
