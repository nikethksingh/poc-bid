import React, { Component } from 'react';
import { connect } from 'react-redux';
import UiSelect from '../../components/aclit_UiSelect';

let selectData = ['Approve', 'DisApprove'];
class Admin extends Component {
    handleChange = (e, i) => {
        let value = e.target.value;
        let obj = { index: i, status: value === 'Approve' ? true : false };
        this.props.updateData(obj);
    };
    render() {
        let { itemData } = this.props.user;
        return (
            <div className="table-wrapper">
                <table className="table-style">
                    <tbody>
                        <tr>
                            <th>Sl. no.</th>
                            <th>Bidder Name</th>
                            <th>Item</th>
                            <th>Quantity</th>
                            <th>Amount</th>
                            <th>Approve/DisApprove</th>
                        </tr>

                        {itemData.map((d, i) => (
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{d.name}</td>
                                <td>{d.item}</td>
                                <td>{d.quantity}</td>
                                <td>{d.bidAmnt}</td>
                                <td>
                                    <UiSelect
                                        value={d.status ? 'Approve' : 'DisApprove'}
                                        onChange={e => this.handleChange(e, i)}
                                        item={selectData}
                                    />
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }
}

let mapStateToProps = state => ({
    auth: state.auth,
    user: state.user,
});

let mapDispatchToProps = dispatch => {
    return {
        updateData: data => dispatch({ type: 'UPDATE_DATA', ...data }),
        login: data => dispatch({ type: 'LOGIN', data: data }),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Admin);
