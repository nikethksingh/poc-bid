import { combineReducers } from "redux";
import { createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import user from "./user";
import auth from "./auth";

let cReducer = combineReducers({
  user: user,
  auth: auth
});

const persistConfig = {
  key: "root",
  storage
};

export default () => {
  const persistedReducer = persistReducer(persistConfig, cReducer);
  let store = createStore(persistedReducer);
  let persistor = persistStore(store);
  return { store, persistor };
};
