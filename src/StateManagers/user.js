let initialState = {
    itemData: [],
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'SAVE_DATA':
            let arr = state.itemData;
            arr.push(action.data);
            return { ...state, itemData: arr };
        case 'UPDATE_DATA':
            let updatArr = state.itemData;
            updatArr[action.index].status = action.status;
            return { ...state, itemData: updatArr };
        default:
            return state;
    }
};
