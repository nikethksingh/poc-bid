let initialState = {
    user: { isAuthenticated: false, role: null },
};

let users = ['ram', 'shayam', 'rohit', 'john', 'andre'];
let admin = ['admin'];

export default (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN':
            let obj = { user: {} };
            obj.user = Object.assign({}, state.user);
            if (users.includes(action.data)) {
                obj.user.isAuthenticated = true;
                obj.user.role = 'user';
                obj.user.name = action.data;
            } else if (admin.includes(action.data)) {
                obj.user.isAuthenticated = true;
                obj.user.role = 'admin';
                obj.user.name = action.data;
            } else {
                obj = { user: { isAuthenticated: false, role: null } };
            }
            return { ...state, ...obj };
        default:
            return state;
    }
};
