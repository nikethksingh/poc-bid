import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import UiButton from '../components/aclit_Button';
import User from '../containers/User';
import Login from '../containers/Login';
import Admin from '../containers/Admin';

let Headers = ({ props }) => (
    <header>
        <b>Bid-Place</b>
        {props.auth.user.isAuthenticated && <UiButton onClick={() => props.login('')} text="logout" />}
    </header>
);

class CustomRoutes extends React.Component {
    checkRoutes = user =>
        !user.isAuthenticated ? (
            <Switch>
                <Route exact path="/" component={Login} />
                <Route render={() => <Redirect to="/" />} />
            </Switch>
        ) : (
            <Switch>
                <Route path="/user" component={User} />
                <Route path="/admin" component={Admin} />
                <Route render={() => <Redirect to={user.role === 'user' ? '/user' : '/admin'} />} />
            </Switch>
        );

    render() {
        let { user } = this.props.auth;
        return (
            <Router>
                <Headers props={this.props} />
                {this.checkRoutes(user)}
            </Router>
        );
    }
}

let mapStateToProps = state => ({ auth: state.auth });

let mapDispatchToProps = dispatch => {
    return {
        login: data => dispatch({ type: 'LOGIN', data: data }),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CustomRoutes);
